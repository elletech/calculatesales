package jp.alhinc.sugao_yuta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) throws IOException {
		//コマンドライン引数に関するエラー処理
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> namesHashmap = new HashMap<String, String>();
		HashMap<String, Long> saleHashmap = new HashMap<String, Long>();//売上げデータの集計


		////支店定義ファイルから読み込んだデータをHashMap<支店番号, 支店名>として所持
		if (!(loadData(args[0], "branch.lst", "支店", "\\A[0-9]{3}\\z", namesHashmap, saleHashmap))) {
			return;
		}


		////売上げファイルから読み込んだデータを元に集計
		//コマンドライン引数のディレクトリにあるファイルを全部読み込んで配列に入れる
		File dir = new File(args[0]);
		File allFiles[] = dir.listFiles();
		ArrayList<File> rcdFiles = new ArrayList<File>();

		//コマンドライン引数のディレクトリにあるファイルで8桁の数字.rcdならrcdFiles配列にいれる
		for (int i = 0; i < allFiles.length; i++) {
			//allFiles[i].isFile()でファイルなのかディレクトリなのかを判定する
			if (allFiles[i].getName().matches("\\A[0-9]{8}.rcd\\z") && allFiles[i].isFile()) {
				rcdFiles.add(allFiles[i]);
			}
		}

		for (int i = 0; i < rcdFiles.size(); i++) {
			if (i != (rcdFiles.size() - 1)) {
				String str1 = rcdFiles.get(i).getName().replace(".rcd", "");
				String str2 = rcdFiles.get(i+1).getName().replace(".rcd", "");
				Integer int1 = Integer.parseInt(str1);
				Integer int2 = Integer.parseInt(str2);
				if ((int2 - int1) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
		}

		//集計作業
		BufferedReader saleBr = null;
		try {
			for (int i = 0; i < rcdFiles.size(); i++) {
				FileReader saleFr = new FileReader(rcdFiles.get(i));
				saleBr = new BufferedReader(saleFr);
				ArrayList<String> fileDataLine = new ArrayList<String>();

				String line;
				while ((line = saleBr.readLine()) != null) {
					fileDataLine.add(line);
				}

				if (fileDataLine.size() <= 1 || fileDataLine.size() >= 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//売上げファイルの支店コードが支店定義ファイルに存在するかどうか
				if (namesHashmap.containsKey(fileDataLine.get(0))){
				}else {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//rcdファイルの2行目がlong型であるかどうかの判定
				long proceeds = 0;
				try {
					proceeds = Long.parseLong(fileDataLine.get(1));
				} catch (NumberFormatException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				if (saleHashmap.containsKey(fileDataLine.get(0))) {
					long setValue = saleHashmap.get(fileDataLine.get(0)) + proceeds;
					if (String.valueOf(setValue).length() <= 10) {//桁数が10桁を超えるかどうかの判定
						saleHashmap.put(fileDataLine.get(0), setValue);
					}else{
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
			}

		} finally {
			if (saleBr != null) {
				try {
					saleBr.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		////支店別売上ファイルの出力
		if (!(outputFile(args[0], "branch.out", namesHashmap, saleHashmap))) {
			return;
		}

	}

	public static boolean loadData (String path, String inputFileName, String name, String conditions, HashMap<String, String> namesHashmap, HashMap<String, Long> saleHashmap) throws IOException {
		BufferedReader namesBr = null;
		try {
			File namesData = new File(path, inputFileName);
			//支店定義ファイルが存在しないとき
			if (!(namesData.exists())) {
				System.out.println(name + "定義ファイルが存在しません");
				return false;
			}
			FileReader namesFr = new FileReader(namesData);
			namesBr = new BufferedReader(namesFr);
			String line;

			while ((line = namesBr.readLine()) != null) {
				String[] split = line.split(",", 0);

				//支店コードが3桁の数値か、一行の要素数が適切かどうかの判定
				if (split[0].matches(conditions) && split.length == 2 ) {
					namesHashmap.put(split[0], split[1]);
					saleHashmap.put(split[0], 0L);//saleHashmapの初期化
				}else {
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}

			return true;

		} finally {
			// 最後にファイルを閉じてリソースを開放する
			// tryの中にcloseを書くと閉じられなくなる可能性があるためfinallyのなかに書く
			if (namesBr != null) {
				try {
					namesBr.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}

	public static boolean outputFile(String path, String outputFileName, HashMap<String, String> namesHashmap, HashMap<String, Long> saleHashmap) throws IOException {
		BufferedWriter bw = null;
		try {
			File file = new File(path, outputFileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(Entry<String, String> names : namesHashmap.entrySet()) {
				for(Entry<String, Long> sale : saleHashmap.entrySet()) {
					if(names.getKey().equals(sale.getKey())) {
						bw.write(names.getKey() + "," + names.getValue() + "," + sale.getValue() + System.lineSeparator());
					}
				}
			}
			return true;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if (bw != null) {
				try {
					bw.close();
				}catch (IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}

}
